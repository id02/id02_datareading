import sys
if sys.version_info < (3,6):
    sys.exit('Sorry, Python < 3.6 is not supported')


from setuptools import setup, find_packages

setup(name='id02data',
      version='0.1',
      python_requires='>3.6',
      description='ESRF ID02 data access',
      author='William Chevremont',
      author_email='william.chevremont@esrf.fr',
      install_requires=[
          'numpy',
          'h5py>=3.4',
          'hdf5plugin',
      ],
      packages=find_packages(include=['id02data', 'id02data.*']),
    )