

classdef ID02NexusFileReader

    properties
        filename
    end
    methods (Access = public)
        function obj = ID02NexusFileReader(filename)
            obj.filename = filename;
        end

        function [axes, data] = read_data(obj, no)
            % Read the data from an NeXuS file generated at id02
            % filename: The filename to read
            % no: If provided, return only the nth curve/image. Return all by
            % default.
            %
            % Return [axes, data] the axes and data name and values.
        
            if nargin == 1
                no = 'all';
            end
        
            ds = obj.sub_get_default_data();
        
            axesds_ = h5readatt(obj.filename, ds, 'axes');
            datads_ = h5readatt(obj.filename, ds, 'signal');
            datads = obj.sub_get_abspath(ds, datads_);
        
            %axesds = {};
            axes = {};
        
            for i = 1:length(axesds_)
                a = axesds_(i);
                if a == '.'
                    continue
                end
        
                axesds = string(obj.sub_get_abspath(ds, a));
        
                axes = [axes; {a{1}, h5read(obj.filename, axesds)}];
        
            end
        
            if no == 'all'
                data = {datads_, h5read(obj.filename, datads)};
            else
                shape = h5info(obj.filename, datads).Dataspace.Size;
                st = ones(1, size(shape,2));
                shape(end) = 1;
                st(end) = no;
            
                data = {datads_, h5read(obj.filename, datads,st ,shape)};
            end
        
        end

        function cnames = get_counter_names(obj)

            % check if MCS folder is close to default data

            ds = obj.sub_get_default_data();

            spath = strsplit(ds, '/');
            mcspath = strcat(strjoin(spath(1:end-1),'/'),'/MCS/interpreted');

            cnames = {};

            try
                infos = h5info(obj.filename, mcspath).Datasets;

                for I = infos
                    cnames = [cnames; {I.Name}];
                end
            catch
                
            end
        end

        function headers = get_headers(obj)
%             default_ds_ = self._get_default_data()
%         
%             for suff in ('../parameters', '../header','header_array'):
%                 headerds = self._get_abs_path(default_ds_, suff)
%                 #print(headerds)
%                 if headerds in self._fd:
%                     break
%             else:
%                 return {}
%             
%             r = {}
%             for k in self._fd[headerds]:
%                 r[k] = self._str(self._fd[f"{headerds}/{k}"][()])
%                 
%             return r

            ds = obj.sub_get_default_data();

            for suff = {'../parameters', '../header', 'header_array'}
                headerpath = obj.sub_get_abspath(ds,suff{1});
                try
                    inf = h5info(obj.filename, headerpath);
                    break
                catch

                end
            end

            headers = {};

            for n = {inf.Datasets.Name}
                path = obj.sub_get_abspath(headerpath, n{1});
                path = path(1:end-1);
                headers{end+1} = {n, h5read(obj.filename, path)};
            end
        end

        function value = get_counter_value(obj, name)

            % check if MCS folder is close to default data

            ds = obj.sub_get_default_data();

            spath = strsplit(ds, '/');
            cdspath = strcat(strjoin(spath(1:end-1),'/'),'/MCS/interpreted/', name);

            value = h5read(obj.filename, cdspath);
        end
        
    end
    methods (Access = protected)
        
        function isnx = sub_is_NXdata(obj, ds)
        
            isnx = h5readatt(obj.filename, ds, "NX_class") == "NXdata";
        
        end
        
        function nds = sub_get_abspath(obj, ds, nds)

            if ds(end) ~= '/'
                ds = strcat(ds, '/');
            end
        
            if nds(1) ~= '/'
                nds = strcat(ds, nds);
            end

            ss = strsplit(nds,'/');

            out = {};

            for s = ss
                s = s{1};
                if length(s) == 0
                    continue
                end
               
                if strcmp(s, '..')
                    out = out(1:end-1);
                else
                    out{end+1} = s;
                end
            end

            nds = cell2mat(strcat(out,'/'));
            nds = ['/',nds];
        end
        
        
        function ds = sub_get_default_data(obj, ds)
        
            if nargin == 1
                ds = '/';
            end
        
            if ~obj.sub_is_NXdata(ds)
                defds = obj.sub_get_abspath(ds, h5readatt(obj.filename, ds, 'default'));
                ds = obj.sub_get_default_data(defds);
            end
        end
    end
end