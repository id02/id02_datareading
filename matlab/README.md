# MatLab data loading

MatLab data loading relies on the ID02NexusFileReader class, found in corresponding .m file.

## How to use
1. Put the ID02NexusFileReader.m file into a folder in the PATH or in your working folder.
2. Reading 1D data:

       >> fr = ID02NexusFileReader('FP_eiger2_00001_00_ave.h5');       
       >> [axes, data] = fr.read_data()
        axes =
        
        1×2 cell array
       
             {'q'}    {1636×1 single}

        data =

        1×2 cell array

             {'data'}    {1636×10 single}

3. Reading 2D data:

		>> fr = ID02NexusFileReader('FP_eiger2_00001_00_azim.h5');
		>> [axes, data] = fr.read_data()

		axes =

		  2×2 cell array

		    {'chi'}    { 360×1 single}
		    {'q'  }    {1000×1 single}


		data =

		  1×2 cell array
		
		    {'data'}    {1000×360×10 single}
4. To get only a single frame/curve, pass the curve number (1-based) as function argument.
		
		>> [axes, data] = fr.read_data(5)
		
		axes =

		  2×2 cell array

		    {'chi'}    { 360×1 single}
		    {'q'  }    {1000×1 single}


		data =

		  1×2 cell array

		    {'data'}    {1000×360 single}

>> 
6. Getting the names of all counters (might be temperature control, position, ... Depending on your experiment. Let's get the transmission for each frame:
		
		>> fr.get_counter_names

		ans =

		  1×35 cell array
		    {'eiger2_roi_coun…'}    {'elev_position'}    {'epoch'}    {'mcs_aux1_raw'}    {'mcs_aux2_raw'}    {'mcs_elapsed_time'}  {'mcs_epoch'}    {'mcs_fastmotor'}    {'mcs_pin1_raw'}    {'mcs_pin3_raw'}    {'mcs_pin41_raw'}    {'mcs_pin42_raw'}  {'mcs_pin4_raw'}    {'mcs_pin6_raw'}    {'mcs_pin7_raw'}    {'mcs_pin8_raw'}    {'mcs_time_raw'}    {'mcs_v1_raw'}  {'mcs_v2_raw'}    {'mcs_v3_raw'}    {'mcs_v4_raw'}    {'sah_position'}    {'sav_position'}    {'scalers_pin1'}  {'scalers_pin3'}    {'scalers_pin4'}    {'scalers_pin41'}    {'scalers_pin42'}    {'scalers_pin6'}    {'scalers_pin7'}  {'scalers_pin8'}    {'scalers_time'}    {'timer_elapsed_t…'}    {'timer_epoch'}    {'trm_counter_trm'}

		>> fr.get_counter_value('trm_counter_trm')

		ans =

		    0.6013
		    0.6024
		    0.6005
		    0.6003
		    0.6002
		    0.6027
		    0.6007
		    0.6018
		    0.6031
		    0.5996
